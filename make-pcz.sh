#!/bin/bash

ROOT_DIR=$( dirname $( which --skip-alias --skip-functions "${0}" ) )

cd "${ROOT_DIR}"

zip -r my-homebrew.pcz homebrewdata/ install.lst

